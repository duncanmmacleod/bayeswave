/*
 *  Copyright (C) 2018 Neil J. Cornish, Tyson B. Littenberg
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */

void average_log_likelihood_via_direct_downsampling(struct Chain *chain, double *ACL, int nPoints);
void average_log_likelihood_via_recursion_downsampling(int M, struct Chain *chain);
void average_log_likelihood_via_ACF(struct Chain *chain);

void SplineThermodynamicIntegration(double *temperature, double *avgLogLikelihood, double *varLogLikelihood, int NC, double *logEvidence, double *varLogEvidence);
void CovarianceThermodynamicIntegration(double *temperature, double **loglikelihood, int M, int NC, char modelname[], double *logEvidence, double *varLogEvidence);
void ThermodynamicIntegration(double *temperature, double **loglikelihood, int M, int NC, char modelname[], double *logEvidence, double *varLogEvidence);
void TrapezoidIntegration(struct Chain *chain, int counts, char modelname[], double *logEvidence, double *varLogEvidence);

void LaplaceApproximation(struct Data *data, struct Model *model, struct Chain *chain, struct Prior *prior, double *logEvidence);
